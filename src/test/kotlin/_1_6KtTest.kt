import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_6KtTest{

    @Test
    fun withNegatives(){
        val result = fun1_6(-55, -27, -48)
        val expected = -65
        assertEquals(expected.toLong(), result)
    }

    @Test
    fun allZero(){
        val result = fun1_6(0, 0, 0)
        val expected = 0
        assertEquals(expected.toLong(), result)
    }

    @Test
    fun negativeAndPositive(){
        val result = fun1_6(-50, 123, 27)
        val expected = 50
        assertEquals(expected.toLong(), result)
    }



}