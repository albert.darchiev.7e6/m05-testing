import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_8KtTest{

    @Test
    fun nextNegative(){
        val result = fun1_8(-250.0)
        val expected = -500.0
        assertEquals(expected, result)
    }

    @Test
    fun nextPositive(){
        val result = fun1_8(-452.0)
        val expected = -904.0
        assertEquals(expected, result)
    }

    @Test
    fun nextOfZero(){
        val result = fun1_8(0.0)
        val expected = 0.0
        assertEquals(expected, result)
    }


}