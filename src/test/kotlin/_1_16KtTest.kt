import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.lang.Double

internal class _1_16KtTest{

    @Test
    fun negative(){
        val result = fun1_16(-4562548)
        val expected = -4562548.0
        assertEquals(expected, result)
    }

    @Test
    fun zero(){
        val result = fun1_16(0)
        val expected = 0.0
        assertEquals(expected, result)
    }

    @Test
    fun positive(){
        val result = fun1_16(158795645158795645)
        val expected = 158795645158795645.0
        assertEquals(expected, result)
    }
}