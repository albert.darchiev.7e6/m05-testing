import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_13KtTest{
    @Test
    fun negative(){
        val result = fun1_13(-147.0, -124.0)
        val expected = -271.0
        assertEquals(expected, result)
    }

    @Test
    fun zero(){
        val result = fun1_13(0.0, 0.0)
        val expected = 0.0
        assertEquals(expected, result)
    }

    @Test
    fun positive(){
        val result = fun1_13(125.0, 138.0)
        val expected = 263.0
        assertEquals(expected, result)
    }
}