import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_10KtTest{
    @Test
    fun negative(){
        val result = fun1_10(-586)
        val expected = 269702.58771803015
        assertEquals(expected, result)
    }

    @Test
    fun zero(){
        val result = fun1_10(0)
        val expected = 0.0
        assertEquals(expected, result)
    }

    @Test
    fun positive(){
        val result = fun1_10(1234)
        val expected = 1195969.7657024448
        assertEquals(expected, result)
    }


}