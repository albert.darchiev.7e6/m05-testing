import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_5KtTest{

    @Test
    fun operationSimple(){
        val result = fun1_5(22, 45, 180,3)
        val expected = 0
        assertEquals(expected.toLong(), result)
    }
    @Test
    fun operationWithNegatives(){
        val result = fun1_5(-852, -7852456, -374523,25)
        val expected = 180626084
        assertEquals(expected.toLong(), result)
    }
    @Test
    fun operationBigNumbers(){
        val result = fun1_5(-852, -785245665465485218, -3745231231,25)
        val expected = 4711473992792916420
        assertEquals(expected, result)
    }
}