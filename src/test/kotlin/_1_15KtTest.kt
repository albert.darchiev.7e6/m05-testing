import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.lang.Double

internal class _1_15KtTest{
    @Test
    fun negative(){
        val result = fun1_15( -8526)
        val expected = -5
        assertEquals(expected, result)
    }

    @Test
    fun zero(){
        val result = fun1_15(0)
        val expected = 1
        assertEquals(expected, result)
    }

    @Test
    fun positive(){
        val result = fun1_15(12356)
        val expected = 57
        assertEquals(expected, result)
    }
}