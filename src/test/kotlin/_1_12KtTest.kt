import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_12KtTest{

    @Test
    fun negative(){
        val result = fun1_12(-235.0)
        val expected = -391.0
        assertEquals(expected, result)
    }

    @Test
    fun zero(){
        val result = fun1_12(0.0)
        val expected = 32.0
        assertEquals(expected, result)
    }

    @Test
    fun positive(){
        val result = fun1_12(125.0)
        val expected = 257.0
        assertEquals(expected, result)
    }
}