import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_11KtTest{

    @Test
    fun negative(){
        val result = fun1_11(-50.0, -60.0, -80.0)
        val expected = -240000.000
        assertEquals(expected, result)
    }

    @Test
    fun zero(){
        val result = fun1_11(0.0, 0.0, 0.0)
        val expected = 0.0
        assertEquals(expected, result)
    }

    @Test
    fun positive(){
        val result = fun1_11(321.0, 123.0, 321.0)
        val expected = 12674043.000
        assertEquals(expected, result)
    }


}