import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.lang.Double.NaN

internal class _1_14KtTest{

    @Test
    fun negative(){
        val result = fun1_14(-125.0, -156.0)
        val expected = 1.248
        assertEquals(expected, result)
    }

    @Test
    fun zero(){
        val result = fun1_14(0.0, 0.0)
        val expected = NaN
        assertEquals(expected, result)
    }

    @Test
    fun positive(){
        val result = fun1_14(148.0, 321.0)
        val expected = 2.168918918918919
        assertEquals(expected, result)
    }

}