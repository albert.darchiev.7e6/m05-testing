import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_2KtTest{

    @Test
    fun multiplySmallNegative(){
        val result = fun1_2(-5)
        val expected = -10
        assertEquals(expected.toLong(), result)
    }

    @Test
    fun multiplyBigNegative(){
        val result = fun1_2(-374522374522)
        val expected = -749044749044
        assertEquals(expected, result)
    }

    @Test
    fun multiplyZero(){
        val result = fun1_2(0)
        val expected = 0
        assertEquals(expected.toLong(), result)
    }
}