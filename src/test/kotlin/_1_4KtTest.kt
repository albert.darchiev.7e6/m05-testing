import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_4KtTest{

    @Test
    fun multiplyNegativePositive(){
        val result = fun1_4(-1234, 54)
        val expected = -66636
        assertEquals(expected.toLong(), result)
    }

    @Test
    fun multiplyBigPositives(){
        val result = fun1_4(23165498, 2316549)
        val expected = 53664011226402
        assertEquals(expected, result)
    }

    @Test
    fun multiplyBigNegatives(){
        val result = fun1_4(-23165498, 2316549)
        val expected = -53664011226402
        assertEquals(expected, result)
    }
}