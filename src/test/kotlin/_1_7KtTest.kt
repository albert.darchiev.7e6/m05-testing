import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_7KtTest{

    @Test
    fun nextNegative(){
        val result = fun1_7(-88)
        val expected = -87L
        assertEquals(expected, result)
    }

    @Test
    fun nextBigPositive(){
        val result = fun1_7(6547896451364579465)
        val expected = 6547896451364579466
        assertEquals(expected, result)
    }

    @Test
    fun nextZero(){
        val result = fun1_7(0)
        val expected = 1L
        assertEquals(expected, result)
    }


}

