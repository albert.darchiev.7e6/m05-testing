import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_9KtTest{

    @Test
    fun negative(){
        val result = fun1_9(-80.0, 125.0)
        val expected = 256.25
        assertEquals(expected, result)
    }

    @Test
    fun positive(){
        val result = fun1_9(125.0, 84.0)
        val expected = 32.800000000000004
        assertEquals(expected, result)
    }

    @Test
    fun positiveAndNegative(){
        val result = fun1_9(125.0, -55.0)
        val expected = 144.0
        assertEquals(expected, result)
    }


}