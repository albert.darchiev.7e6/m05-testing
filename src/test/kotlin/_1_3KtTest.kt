import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_3KtTest{

    @Test
    fun multiplyNegativePositive(){
        val result = fun1_3(1500, -1600)
        val expected = -100
        assertEquals(expected.toLong(), result)
    }

    @Test
    fun multiplyBigPositives(){
        val result = fun1_3(23165498, 2316549)
        val expected = 25482047
        assertEquals(expected.toLong(), result)
    }

    @Test
    fun multiplyBigNegatives(){
        val result = fun1_3(-23165498, 2316549)
        val expected = -20848949
        assertEquals(expected.toLong(), result)
    }

}
//sd