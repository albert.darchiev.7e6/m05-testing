/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/19
* TITLE: 1.8 Dobla el decimal
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero:")
    val numero1 = scanner.nextDouble()

    print("El seguent numero es el ")
    print(numero1 * 2)

}

fun fun1_8(num: Double): Double {
    return (num*2)
}