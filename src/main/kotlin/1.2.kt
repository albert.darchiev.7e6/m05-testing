/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/19
* TITLE: 1.2 Dobla l’enter
*/
import java.util.*
fun main(userInputValue: Int) {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    println("Aquest és el número introduït x 2: ")
    println( userInputValue*2)
}

fun fun1_2(userInputValue: Long): Long {
    return (userInputValue*2)
}
//