/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/19
* TITLE: 1.7 Número següent
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix un numero:")
    val numero1 = scanner.nextInt().toLong()

    print("El seguent numero es el ")
    print(fun1_7(numero1))

}

fun fun1_7(num: Long): Long {
    return (num+1)
}
