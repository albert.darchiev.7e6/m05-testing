/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/19
* TITLE: 1.6 Pupitres
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix el primer numero:")
    val numero1 = scanner.nextInt()

    println("Introdueix el segon numero:")
    val numero2 = scanner.nextInt()

    println("Introdueix el tercer:")
    val numero3 = scanner.nextInt()

    println("El teu apartament te una mesura de:")
    val residuo = ((numero1 + numero2 + numero3) % (2))
    println(((numero1 + numero2 + numero3) / (2)) + (residuo))

}
fun fun1_6(num1: Long, num2: Long, num3: Long): Long {
    val residuo = (num1+num2+num3) % 2
    return (((num1+num2+num3) / 2) + residuo)
}