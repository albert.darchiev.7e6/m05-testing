/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/19
* TITLE: 1.13 Quina temperatura fa?
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    println("num1:")
    val numero1 = scanner.nextDouble()

    println("num2:")
    val numero2 = scanner.nextDouble()

    print("Temperatura actual: ")
    print (numero1 + numero2)
}

fun fun1_13(num1: Double, num2:Double):Double{
    return (num1+num2)
}