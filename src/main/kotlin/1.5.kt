/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/19
* TITLE: 1.5 Operació boja
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix el primer numero:")
    val num1 = scanner.nextInt()
    println("Introdueix el segon numero:")
    val num2 = scanner.nextInt()
    println("Introdueix el tercer:")
    val num3 = scanner.nextInt()
    println("Introdueix el quart:")
    val num4 = scanner.nextInt()
    println("El teu apartament te una mesura de:")
    println((num1 + num2) * (num3 % num4))
}
fun fun1_5(num1: Long, num2: Long, num3:Long, num4:Long): Long {
            return ((num1 + num2) * (num3 % num4))
    }