import java.util.Scanner

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/19
* TITLE: 1.3 Suma de dos nombres enters
*/
fun main() {
    val scanner = Scanner(System.`in`)
    val numero1 = scanner.nextInt()
    val numero2 = scanner.nextInt()
    println("Aquesta és la suma dels 2 números introduits:")
    println(numero1+numero2)
}

fun fun1_3(numero1: Long, numero2: Long): Long {
    return (numero1 + numero2)
}