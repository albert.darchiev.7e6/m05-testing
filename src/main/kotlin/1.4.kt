/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/19
* TITLE: 1.4 Calcula l’àrea
*/
import java.util.*
fun calculArea(){
    val scanner = Scanner(System.`in`)
    val num1 = scanner.nextInt()
    val num2 = scanner.nextInt()
    println("El teu apartament te una mesura de:")
    println(num1*num2)
}

fun fun1_4(num1: Long, num2: Long): Long{
    return (num1*num2)
}
