/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/21
* TITLE: 1.14 Divisor de compte
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    println("num1:")
    val numero1 = scanner.nextDouble()

    println("num2:")
    val numero2 = scanner.nextDouble()

    print("Resultat: ")
    print (numero2 / numero1)
    print("€")
}

fun fun1_14(num1:Double, num2:Double):Double{
    return (num2/num1)
}